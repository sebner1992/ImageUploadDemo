package main;
import java.awt.Button;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class LoadImageApp extends Component
{

	BufferedImage img;

	public void paint(Graphics g)
	{
		scaleImage(img);
		g.drawImage(img, 0, 0, null);
	}

	public LoadImageApp()
	{
		try
		{
			File file = new File("d:/repository/ImageUploadDemo/src/main/kirby.jpg");
			img = ImageIO.read(file);
			scaleImage(img);
		} catch (IOException e)
		{
			System.out.println("CRAP");
		}

	}

	public Dimension getPreferredSize()
	{
		if (img == null)
		{
			return new Dimension(100, 100);
		} else
		{
			return new Dimension(img.getWidth(null), img.getHeight(null));
		}
	}

	public static void main(String[] args)
	{

		JFrame f = new JFrame("Load Image Sample");

		f.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});

		f.add(new LoadImageApp());
		f.pack();
		f.setVisible(true);
	}
	
	public static BufferedImage scaleImage(BufferedImage img)
	{
		BufferedImage before = img;
		int w = before.getWidth();
		int h = before.getHeight();
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(0.5, 0.5);
		AffineTransformOp scaleOp = 
		   new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		after = scaleOp.filter(before, after);
		return after;
	}
}
